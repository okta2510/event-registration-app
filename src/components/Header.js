import Image from 'next/image'

const Head = ({ children }) => (
  <div className="container  mx-auto pb-5 pt-10 text-center">
    <header className='text-center flex'>
    <Image
      src="/logo-jakarta.png"
      alt="Logo"
      className="mx-auto mb-3"
      width={91}
      height={24}
      priority></Image>
    <Image
      src="/logo.png"
      alt="Logo"
      className="mx-auto mb-3"
      width={91}
      height={24}
      priority></Image>
    </header>
    <main>{children}</main>
  </div>
);

export default Head;