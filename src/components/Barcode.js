import Image from 'next/image'

const Barcode = ({ children, userInfo, isLoading }) => {

  return (
    <>
      <div className="mx-auto pt-5 pb-6 text-center bg-white w-fit text-gray-800 px-6 rounded-md shadow-2xl">
        <div className={`text-center ${userInfo && Object.keys(userInfo).length > 0 ? '' : 'hidden'}`}>
          <p className='mb-3 text-lg text-gray-400'>Scan QR Code di Venue</p>
          {/* <Image
            src={userInfo ? userInfo.qrcode : '/no-image.jpg'}
            alt="Logo"
            className="mx-auto mb-3"
            width={150}
            height={24}
            priority></Image> */}
            <iframe  className="mx-auto mb-3 text-center" style={{
              width: '318px',
              height: '320px'
            }} src={userInfo.qrcode}></iframe>
    
          <p className='font-bold mb-1 text-sky-700 text-xl'>{userInfo.name}</p>
          <small className='block font-bold text-xl mb-3'>{`CATEGORY ${userInfo.cat}`}</small>
          <small className='block mt-1'>{0+userInfo.phone}</small>
          <small className='block mb-2'>{userInfo.email}</small>
        </div>
        <div className={`text-center text-gray-400 py-10 ${userInfo && Object.keys(userInfo).length === 0 ? '' : 'hidden'}`}>
        {isLoading ? 'Memuat Data...' : '- No Data - '}
        </div>
        <main>{children}</main>
      </div>
    </>
  )
};

export default Barcode;