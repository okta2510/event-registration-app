// components/LoginForm.js
import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';

const PUBLIC_LOGIN_URL = process.env.NEXT_PUBLIC_LOGIN_URL
const LoginForm = () => {
  const router = useRouter();

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [logged, setLogged] = useState(false);

  useEffect(() => {
    checkToken()
  }, []);

  useEffect(() => {
    checkToken()
  }, [logged]);

  const checkToken = () => {
    const token = localStorage.getItem('token');
    if(token) {
      setLogged(true)
    } else {
      setLogged(false)
    }
  };

  const handleRedirect = () => {
    // Redirect to the '/dashboard' page
    router.push('/scan');
  };

  const handleUsernameChange = (e) => {
    setUsername(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };
  
  const handleLogout = async () => {
    localStorage.removeItem('token')
    setLogged(false)
  }
  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await fetch(PUBLIC_LOGIN_URL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email: username, password }),
      }).then((response) => response.json()).then((json) => {
        return json
      });
      console.log(response)
      if (response.status_code === 200) {
        // Handle successful login
        await localStorage.setItem('token', response?.data?.token)
        setLogged(true)
        console.log('Login successful');
      } else {
        // Handle unsuccessful login
        alert('Login Gagal')
        console.error('Login failed');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <>
      <div>
        {!logged  ? (
          <form className='text-black' onSubmit={handleSubmit}>
          <div className='mb-2 gap-2'>
            <label className="pr-2" htmlFor="username">Username:</label>
            <input
              type="text"
              id="username"
              value={username}
              onChange={handleUsernameChange}
            />
          </div>
          <div className='mb-2'>
            <label className="pr-2" htmlFor="password">Password:</label>
            <input
              type="password"
              id="password"
              value={password}
              onChange={handlePasswordChange}
            />
          </div>
          <div>
            <button className='w-64 py-1 px-2 roundeds bg-cyan-800 rounded-md  text-md pl-10 pr-10 py-4 text-white' type="submit">Submit</button>
          </div>
        </form>
        ):(
          <div>
          <button className='inline-block w-64 py-1 px-2 roundeds bg-red-600 text-white rounded-md  text-md pl-10 pr-10 py-4 text-black mb-2' type="button" onClick={handleLogout}>Logut</button>
          <p > <Link className='inline-block w-64 py-1 px-2 roundeds bg-white text-black rounded-md  text-md pl-10 pr-10 py-4 text-sky-950' href="/scan">Mulai Scan Profile</Link></p>
          </div>
        )}
      </div>
    </>
  );
};

export default LoginForm;