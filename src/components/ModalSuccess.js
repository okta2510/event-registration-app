import React from 'react';
import Modal from 'react-modal';
import Image from 'next/image'


Modal.setAppElement('#__next');

const MyModal = ({ isOpen, onRequestClose, contentLabel, classProps, isSuccess, userInfo }) => {
  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      contentLabel={contentLabel}
      style={{
            overlay : {
                backgroundColor: null
            },
            content : {
                top: null,
                left: null,
                right: null,
                bottom: null,
                border: null,
                background: null,
                borderRadius: null,
                padding: null,
                position: null
            }
        }}
      overlayClassName={'modal-component '+ classProps }
    >
      {/* Your modal content goes here */}
      <div className='container container-modal-content text-center mx-auto'>
        <h2 className='mb-4 text-5xl font-bold'>{contentLabel}</h2>
        <p className={`w-4/5 mx-auto text-center text-lg ${isSuccess ? 'hidden' : ''}`}>Mohon maaf terjadi kesalahan, Silahkan coba kembali.</p>
        <div className={userInfo && Object.keys(userInfo).length === 0 ? '': 'hidden'}>
          {userInfo ? userInfo.name : ''}
        </div>
        <h1 className={`welcoming-name drop-shadow-2xl text-9xl font-bold ${userInfo && userInfo.name && isSuccess ? '' : 'hidden'}`}>
          {userInfo ? userInfo.name :''}
        </h1>

        <hr className={`mt-7 mb-2 drop-shadow-2xl w-3/6 mx-auto ${userInfo && userInfo.name && isSuccess ? '' : 'hidden'}`}></hr>
        
        <p className={`text-center drop-shadow-2xl text-6xl font-bold mt-3 ${userInfo && userInfo.name && isSuccess ? '' : 'hidden'}`}>
        {userInfo ? `Kategori: ${userInfo.cat}` :''}
        </p>

        <Image
        src="/text-campaign.png"
        alt="Logo"
        className={`mt-16 mx-auto mb-3 ${userInfo && Object.keys(userInfo).length > 0 ? '': 'hidden'}`}

        width={400}
        height={24}
        priority></Image>

        <button className={` mt-10 roundeds bg-neutral-50 rounded-md  text-md font-semi-bold px-5 py-2 ${isSuccess ? 'text-slate-500 mt-5' : 'text-rose-500 mt-10'}`} onClick={onRequestClose}>Selanjutnya...</button>
      </div>
    </Modal>
  );
};

export default MyModal;