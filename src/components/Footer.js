import Image from 'next/image'

const Footer = ({ children }) => (
  <div className="container  mx-auto text-center pt-5 mb-16 text-indigo-950 footer-content">
    <header>
      <Image
        src="/text-campaign.png"
        alt="Logo"
        className="mx-auto mb-3"
        width={500}
        height={24}
        priority></Image>
      {/* Your header content goes here */}
      {/* <h1 className="text-4xl">PAMANAH FUTURE LEADER<br/><strong>PAM JAYA 2023</strong></h1> */}
    </header>
    <main>{children}</main>
  </div>
);

export default Footer;