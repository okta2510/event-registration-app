import React, {
  useState,
  useEffect
} from "react";
import QrReader from "react-web-qr-reader";
import '../app/globals.css'
import Header from "@/components/Header"
import Footer from "@/components/Footer"
import MyModal from '../components/ModalSuccess';
import { useRouter } from 'next/router';

const REGISTRATION_URL = process.env.NEXT_PUBLIC_REGISTRATION_URL
const MODAL_TIMEOUT = process.env.NEXT_PUBLIC_MODAL_TIMEOUT
let inFocus = false
const onFocus = () => {
  inFocus = true;
  // console.log("Tab is in focus", inFocus);
};

// User has switched away from the tab (AKA tab is hidden)
const onBlur = () => {
  inFocus= false;
  // console.log("Tab is blurred", inFocus);
};


const ScanBarcode = () => {
  const router = useRouter();
  const delay = 1000;
  const resolution = 1000
  const modalClosedTimeOut = MODAL_TIMEOUT
  const previewStyle = {
    width: 320
  };

  const [result, setResult] = useState(null);
  const [gunResult, setGunResult] = useState(null);
  const [key, setKey] = useState(0); // State key to force component remount
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const [isCamOff, setCamOff] = useState(false);
  const [isLoading, setLoadingApi] = useState(false);

  const handleRedirect = (path) => {
    // Redirect to the '/dashboard' page
    router.push(path);
  };

  const isJsonString = function (str){
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
  }

  const openModal = () => {
    setModalIsOpen(true);
    setTimeout(() => {
      closeModal();
    }, modalClosedTimeOut); // 2000 milliseconds (2 seconds) delay
  };

  const closeModal = () => {
    // resetResultState()
    setModalIsOpen(false);
  };

  const checkToken = () => {
    const token = localStorage.getItem('token');
    if(!token) {
      handleRedirect('/welcome')
    }
  };
  
  useEffect(() => {
    checkToken();
    window.addEventListener("focus", onFocus);
    window.addEventListener("blur", onBlur);
    // setIsSuccess(true)
    // setResult({name: 'raul gonzales', cat: '3'})
    // openModal()
    // Calls onFocus when the window first loads
    onFocus();
    // Specify how to clean up after this effect:
    checkToken();

    const handleKeyPress = (event) => {
      // Check if the event is coming from the barcode scanner (you may need to identify the key code)
      // Update the state with the scanned data

      setGunResult((prevData) => {
        if (!inFocus) return;
    
        if (event.keyCode === 13) {
          // Enter key pressed, store the new value
          if(isJsonString(prevData)) {
            const obj = JSON.parse(prevData)
            callApi(obj)
          }
          return '';
        } else if (event.keyCode === 123) {
          // Reset state when keyCode is 123
          return event.key;
        } else {
          // Append the key to the existing data
          return prevData === null ? event.key : prevData + event.key;
        }
      });
    };

    // Add event listener when the component mounts
    window.addEventListener('keypress', handleKeyPress);

    // Clean up the event listener when the component unmounts
    return () => {
      window.removeEventListener('keypress', handleKeyPress);
      window.removeEventListener("focus", onFocus);
      window.removeEventListener("blur", onBlur);
    };
  }, []); // Empty dependency array to run the effect only once
  
  
  const callApi = async (val) => {  
    // openModal()
    if(isLoading || modalIsOpen) return
    setLoadingApi(true)
    const token = localStorage.getItem('token')
    console.log(val)
    try {
      const response = await fetch(REGISTRATION_URL+`/${val.id}`, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-type': 'application/json',
          'authorization': `Bearer ${token}`
        },
        // body: val,
      });
      console.log(response)
      if (response.ok) {
        // Handle successful login
        setIsSuccess(true)
        setResult(val);
        console.log(result)
        console.log('Login successful');
      } else {
        // Handle unsuccessful login
        console.error('Login failed');
      }
      await openModal()
      await setLoadingApi(false)
    } catch (error) {
      setIsSuccess(false)
      console.error('Error:', error);
      await openModal()
      await setLoadingApi(false)
    }
  }
  const resetResultState = () => {  
    setResult(null)
  }

  
  const  handleScan = async (res) => {
    if(!inFocus) return
    await resetResultState()
    if (res) {
      // setResult(res.data);
      console.log(res.data)
      if(isJsonString(res.data)) {
        const obj = JSON.parse(res.data)
        callApi(obj)
      }
    }
  };

  const handleError = (error) => {
    console.log(error);
    if(error.name && error.name === "NoVideoInputDevicesError")  setCamOff(true)
  };
  const handleLoad = (e) => {
    console.log('QR Code reader is loaded', e);
  }

  const handleClick = () => {
    resetResultState()
    setKey((prevKey) => prevKey + 1);
    console.log('Button clicked!');
    // Add your desired actions here
  };

  return (
    <>
    <Header/>
      <div className="container mx-auto text-center mb-5">
      
        <QrReader
          key={key}
          className={`mx-auto mb-3 component-scanner ${inFocus ? 'active' : ''} ${ +isCamOff ? 'offline-cam' : ''}`}
          delay={delay}
          resolution={resolution}
          style={previewStyle}
          onLoad={handleLoad}
          onError={handleError}
          onScan={handleScan}
        />
        {/* <p>{result ? 'success' : 'No Result'}</p> */}
        <button className="py-2 px-4 roundeds bg-white text-blue-900 rounded-md" onClick={handleClick}>Scan Ulang</button>
        <p className="text-2xl my-0 text-stone-950 text-loading">{isLoading ? 'Mendaftarkan, Mohon Tunggu...' : ''}</p>
        {result ? (
          <div className="text-xs hidden">
            <h1>Data Sebelumnya:</h1>
            <p>{result.name }</p>
            <p>{result.email }</p>
            <p>{result.phone }</p>
          </div>
        ) : (
          <p></p>
        )}
        {/* {''+isLoading+isCamOff} */}
      </div>
      <Footer/>
      <MyModal
      id="myModal"
        isOpen={modalIsOpen}
        isSuccess={isSuccess}
        userInfo={result}
        onRequestClose={closeModal}
        classProps={isSuccess ? 'active' : 'error'}
        contentLabel={isSuccess ? 'Selamat Datang,' : 'Terjadi Kesalahan'}
        shouldCloseOnOverlayClick={false}
      ></MyModal>
    </>
  );
};

export default ScanBarcode;
