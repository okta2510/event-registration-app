import Image from 'next/image'
import '../app/globals.css'
import Header from "@/components/Header"

export default function Home() {
  return (
    <>
      <div style={{
        paddingTop: '100px'
      }}>
        <Header className="mb-10"/>
        <Image
        src="/text-campaign.png"
        alt="Logo"
        className="mx-auto mb-3 w-3/6 mt-10"
        width={600}
        height={200}
        priority></Image>
      </div>
    </>
  )
}
