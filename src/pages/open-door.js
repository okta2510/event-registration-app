import React, {
  useState,
  useEffect
} from "react";
import Image from 'next/image'
import '../app/globals.css'
import Header from "@/components/Header"
import Footer from "@/components/Footer"
import LoginForm from "@/components/LoginForm"

const GET_BARCODE = process.env.NEXT_PUBLIC_GET_BARCODE
export default function Home() {
  const [userInfo, setUserInfo] = useState({});
  const [isLoading, setLoadingApi] = useState(false);

  useEffect(() => {
    callApi()
    return () => {
    };
  }, []);

  const callApi = async function() {
    if(isLoading) return
    setLoadingApi(true)

    
    setLoadingApi(false)
  }

  return (
    <>
      <Header/>
      <div className="container text-center py-8 my- mx-auto ">
        <h1 className="text-black text-2xl mb-3 uppercase">ADMIN</h1>
        <LoginForm />
      </div>
      <Footer/>
    </>
  )
}
