import React, {
  useState,
  _useEffect
} from "react";
import '../app/globals.css'
import Header from "@/components/Header"
import Footer from "@/components/Footer"

const Example = () => {

  return (
    <>
      <div className="pt-16 mt-16">
        <Header/>
          <div className="container mx-auto text-center">
            {/* <h1>welcome</h1> */}
          </div>
        <Footer/>
      </div>
    </>
  );
};

export default Example;
