import React, {
  useState,
  useEffect
} from "react";
import Image from 'next/image'
import '@/app/globals.css'
import Header from "@/components/Header"
import Footer from "@/components/Footer"
import Barcode from "@/components/Barcode"
import { useRouter } from 'next/router';

const GET_BARCODE = process.env.NEXT_PUBLIC_GET_BARCODE
export default function Home() {
  const router = useRouter();
  const { id } = router.query;
  const [userInfo, setUserInfo] = useState({});
  const [isLoading, setLoadingApi] = useState(false);

  useEffect(() => {
    if(id)callApi()
    return () => {
    };
  }, [id]);

  const callApi = async function() {
    if(isLoading) return
    setLoadingApi(true)

    // const paramsObject = {
    //   param1: 'value1',
    //   param2: 'value2',
    // };

    // const queryParams = new URLSearchParams(paramsObject).toString();
    
    try{
      const response = await fetch(GET_BARCODE+`/${id}`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-type': 'application/json; charset=UTF-8',
        },
      }).then((response) => response.json()).then((json) => {
        return json
      })
      console.log(response)
      if (response.code === 200) {
        // Handle successful login

        setUserInfo(response.data || {})
        console.log('Login successful');
      } else {
        // Handle unsuccessful login
        console.error('Login failed');
      }
    }catch(e){
      console.log(e)
    }
    setLoadingApi(false)
  }

  return (
    <>
      <Header/>
      <div className="container text-center py-8 mx-auto">
        <Barcode userInfo={userInfo} isLoading={isLoading}/>
      </div>
      <Footer/>
    </>
  )
}
