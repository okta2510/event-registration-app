/** @type {import('next').NextConfig} */
const nextConfig = {
  // output: 'export',
  images: {
    domains: ['api.goksregistration.com'],
    remotePatterns: [
      {
        protocol: "https",
        hostname: "**",
      },
    ],
  },
  env: {
    // BASE_URL: process.env.NEXT_PUBLIC_REGISTRATION_URL,
  }
}

module.exports = nextConfig
